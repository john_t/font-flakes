{
  description =
    "A flake giving access to fonts that I use, outside of nixpkgs.";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        defaultPackage = pkgs.symlinkJoin {
          name = "myfonts-0.1.0";
          paths = builtins.attrValues
            self.packages.${system}; # Add font derivation names here
        };

        packages.bodoni-star = pkgs.stdenvNoCC.mkDerivation {
          name = "bodoni-star-font";
          dontConfigue = true;
          src = pkgs.fetchzip {
            url =
              "https://github.com/indestructible-type/Bodoni/releases/download/2.3/Bodoni-master.zip";
            sha256 = "sha256-0g1InTD5J4UU7T0JuWogpIET8TXBh+V0W9DFzj9h9N0=";
            stripRoot = false;
          };
          installPhase = ''
            mkdir -p $out/share/fonts
            cp -R $src/Bodoni-master/fonts/ttf $out/share/fonts/opentype
            cp -R $src/Bodoni-master/fonts/variable $out/share/fonts/VF
          '';
          meta = { description = "Bodoni* by indestructible type*"; };
        };
      });
}
